package main

import (
	"flag"
	"fmt"
	"gitlab.com/pnathan/multivariate-holt-winters-go/mhw"
)

func main() {
	// Define a command-line flag
	csvPath := flag.String("csv", "", "Path to the CSV file containing the data")
	flag.Parse()

	// Check if the CSV file path was provided
	if *csvPath == "" {
		fmt.Println("CSV file path is required. Usage: -csv=path/to/your/data.csv")
		return
	}

	// Create an iterator for the CSV file
	iterator := mhw.NewCSVIterator(*csvPath)
	defer iterator.Close()

	// Define the parameters for the HoltWinters model
	// These parameters may need to be adjusted based on your specific data and requirements
	alpha := 0.4
	beta := 0.3
	gamma := 0.3
	seasonLength := 4
	dimensions := 2 // Adjust based on the number of dimensions in your CSV data

	// Initialize the HoltWinters model
	model := mhw.NewHoltWintersModel(alpha, beta, gamma, seasonLength, dimensions)

	// Fit the model using the data from the CSV file
	model.Fit(iterator)

	// Once the model is fitted, you can use it to make forecasts
	// For example, forecast the next 4 data points
	forecastSteps := 4
	forecast := model.Forecast(forecastSteps)
	fmt.Println("Forecasted values:", forecast)
}
