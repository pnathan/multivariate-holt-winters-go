# multivariate holt-winters in Go

This is a small library designed to allow feeding high volumes of data into a forecasting function to forecast. Ideally, this is suitable for alerts on the 1-300 second scale from a Prometheus type data source.

It is not intended to be a general purpose statistical tool with many knobs. 

# Stability

pre-alpha.

Needs:

- [ ] Unit tests
- [ ] Mathematical validation vs textbook
- [ ] Sanity Checking



# License: LGPL

https://www.gnu.org/licenses/gpl-faq.en.html#LGPLStaticVsDynamic
