package mhw

import (
	"encoding/csv"
	"io"
	"log"
	"os"
	"strconv"
)

type CSVIterator struct {
	file   *os.File
	reader *csv.Reader
	done   bool
}

func NewCSVIterator(filePath string) *CSVIterator {
	file, err := os.Open(filePath)
	if err != nil {
		log.Fatalf("Failed to open CSV file: %s", err)
	}

	return &CSVIterator{
		file:   file,
		reader: csv.NewReader(file),
		done:   false,
	}
}

func (c *CSVIterator) Next() []PointND {
	if c.done {
		return nil
	}

	var points []PointND
	for {
		record, err := c.reader.Read()
		if err == io.EOF {
			c.done = true
			break
		}
		if err != nil {
			log.Fatalf("Failed to read from CSV: %s", err)
		}

		var dimensions []float64
		for _, value := range record {
			number, err := strconv.ParseFloat(value, 64)
			if err != nil {
				log.Fatalf("Failed to parse float: %s", err)
			}
			dimensions = append(dimensions, number)
		}

		points = append(points, PointND{Dimensions: dimensions})
	}

	return points
}

func (c *CSVIterator) Done() bool {
	return c.done
}

func (c *CSVIterator) Close() {
	if err := c.file.Close(); err != nil {
		log.Fatalf("Failed to close CSV file: %s", err)
	}
}
